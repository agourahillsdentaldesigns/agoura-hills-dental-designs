We offer a wide variety of services for patients throughout the Agoura Hills California area. Our patients depend on Dr. Rahimi to meet all of their oral healthcare needs. Whether you need preventative care, restorative solutions or a smile makeover, we can help.

Address: 28632 Roadside Dr, #270, Agoura Hills, CA 91301, USA

Phone: 818-706-6077

Website: [https://agourahillsdentaldesigns.com](https://agourahillsdentaldesigns.com)
